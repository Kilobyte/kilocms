<?php
    /*
    * Tasks:
    * Load modules
    */
    global $rootdir, $db, $p, $pagename, $cfg;
    define("KILOCMS_VERSION", "0.4.0");
    $rootdir = dirname(__FILE__).DIRECTORY_SEPARATOR;
    include_once($rootdir."include".DIRECTORY_SEPARATOR."pageapi.inc.php");
    $d = dir($rootdir."include");
    while ($n = $d->read()) {
        if ($n != "." and $n != "..") {
            include_once($rootdir."include".DIRECTORY_SEPARATOR.$n);
        }
    }
    if ($cfg['maintenancemessage'] != "") {
        page::setTitle("Under Maintenace");
        page::addContents($cfg['maintenancemessage']);
        page::disableEverything();
        page::output();
        die();
    }
    $db = new mysql();
    user::init();
    
    $d = dir($rootdir."modules");
    while ($n = $d->read()) {
        if ($n != "." and $n != "..") {
            if (file_exists($rootdir."modules".DIRECTORY_SEPARATOR.$n.DIRECTORY_SEPARATOR.$n.".php")) {
                include_once($rootdir."modules".DIRECTORY_SEPARATOR.$n.DIRECTORY_SEPARATOR.$n.".php");
            }
        }
    }
    
    if (isset($_GET['p'])) {
        $p = $_GET['p'];
    } else {
        $p = "start";
    }
    $pagename = $p;
    
    if (!pageapi::callPage($p)) {
        $res = $db->query("SELECT * FROM pages WHERE page_id = '".sql_escape($p)."'");
        if ($tmp = $res->fetchObject()) {
            page::setTitle($tmp->page_title);
            if (user::hasPerm("writer")) {
                $tmp2 = '<div class="btn-group pull-right"><a href="?p=admin&sub=editpage&page='.urlencode($p).'" class="btn btn-primary">Edit</a>';
                $dis = false;
                if ($p == "start") {
                    $dis = true;
                }
                $tmp2 .= '<a '.($dis ? '' : 'href="?p=admin&sub=delpage&page='.urlencode($p).'"').' class="btn btn-danger'.($dis ? " disabled" : "").'">Delete</a></div><br>&nbsp;';
                page::addContents($tmp2);
            }
            $tmp->page_visitcount++;
            page::addContents(page::loadTemplate("page.html", array(
                "content" => $tmp->page_content,
                "user" => $tmp->page_lasteditedby,
                "date" => strftime("%d.%m.%Y - %H:%M", $tmp->page_lastediteddate),
                "counter" => $tmp->page_visitcount.($tmp->page_visitcount == 1 ? " time" : " times")
            )));
            $res = $db->query("UPDATE pages SET page_visitcount = $tmp->page_visitcount WHERE page_id = '".sql_escape($p)."'");
            page::addBC($cfg['project'], "?p=start");
            page::addBC($tmp->page_title, null, true);
        } else {
            page::setTitle("404 Not Found");
            header("HTTP/1.1 404 Not Found");
            if (user::hasPerm("writer")) {
                page::addContents("You can now <a href=\"?p=admin&sub=newpage&page=".urlencode($p)."\">create</a> the page.");
            }
        }
    }
    
    page::output();
?>