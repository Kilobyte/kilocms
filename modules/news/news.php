<?php
    class news_displayPage {
        public function run() {
            global $db, $cfg;
            $mode = "view";
            if (isset($_GET['mode'])) $mode = $_GET['mode'];
            page::addBC($cfg['project'], "?p=start", false);
            switch ($mode) {
            case "view":
                $tmp = "";
                $page = 1;
                if (isset($_GET['page'])) $page = $_GET['page'];
                if ($page <= 0) $page = 1;
                $start = ($page - 1) * $cfg['pagelistlimit'];
                page::setTitle("News");
                $res = $db->query("SELECT * FROM news ORDER BY news_date DESC LIMIT $start, ".$cfg['pagelistlimit']);
                while ($el = $res->fetchObject()) {
                    if ($tmp != "") $tmp .= "<hr>";
                    $tmp .= "<h3><a href=\"?p=news&mode=detail&id=$el->news_numid\">$el->news_title</a></h3>";
                    $tmp .= "<div style=\"margin-left: 20px;\">$el->news_description<br>";
                    
                    $tmp .= "<span class=\"muted\"><i>by $el->news_author, ".utils::getPostDate($el->news_date)."</i></span></div>";
                }
                if ($tmp == "")
                    $tmp = "<i>No Elements avaible</i>";
                page::addContents($tmp);
                page::addBC("View News", null, true);
                break;
            case "detail":
                if (!isset($_GET['id'])) {
                    header("Location: ?p=news");
                    return;
                }
                $id = (int) $_GET['id'];
                //page::setTitle("News");
                page::addBC("View News", "?p=news");
                $res = $db->query("SELECT * FROM news WHERE news_numid = ".sql_escape($id));
                if ($el = $res->fetchObject()) {
                    page::setTitle($el->news_title);
                    $author = $el->news_author;
                    $date = utils::getPostDate($el->news_date);
                    $text = $el->news_body;
                    page::addBC($el->news_title, null, true);
                    page::addContents("<span class=\"muted\"><i>Posted by $author, $date</i></span><br>$text");
                } else {
                    page::setTitle("Invalid entry");
                }
            }
        }
    }
    pageapi::registerPage("news", new news_displayPage());
?>