<?php
  class utils {
    private static $randomcnt = 0;
    public static function getPageSelector($cur, $count, $rawlink) {
      $ret = "<div class=\"btn-group\">";
      $dis = "";
      if ($cur <= 1) 
        $dis = " disabled";
      $href = str_replace("[[\$page]]", "1", $rawlink);
      $ret .= "<a class=\"btn$dis\" ".($cur <= 1 ? "" : "href=\"$href\"").">&laquo;</a>";
      $sta = ($cur < 5 ? 1 : $cur - 5);
      $end = ($cur + 5 > $count ? $count : $cur + 5);
      for ($a = $sta; $a <= $end; $a++) {
        $href = str_replace("[[\$page]]", $a, $rawlink);
        $ret .= "<a class=\"btn".($a == $cur ? " btn-primary" : "")."\" href=\"$href\">$a</a>";
      }
      $href = str_replace("[[\$page]]", $count, $rawlink);
      $dis = "";
      $ret .= "<a class=\"btn disabled\">Page $cur of $count</a>";
      if ($cur >= $count) $dis = " disabled";
      $ret .= "<a class=\"btn$dis\" ".($cur >= $count ? "" : "href=\"$href\"").">&raquo;</a></div>";
      return $ret;
    }
    
    public static function bytes2string($bytes) {
      if ($bytes == 1)
        return "1 Byte";
      elseif ($bytes < 1024) 
        return $bytes." Bytes";
      elseif ($bytes < 1024^2)
        return (floor($bytes/1024))." kB";
      elseif ($bytes < 1024^3)
        return (floor($bytes/1024^2))." MB";
      elseif ($bytes < 1024^4)
        return (floor($bytes/1024^3))." GB";
      else
        return (floor($bytes/1024^4))." TB";
    }
    
    public static function getSecureRandom() {
      global $cfg;
      if ($cfg['use_openssl']) {
        $ret = openssl_random_pseudo_bytes(20);
      } else {
        $ret = hash("sha512", time().self::$randomcnt);
        self::$randomcnt++;
      }
    }
    
    public static function sendMail($subject, $template, $receiver, $data = array()) {
      global $cfg;
      $text = page::loadTemplate("mail".DIRECTORY_SEPARATOR.$template, $data);
      $text = str_replace("\n.", "\n..", $text);
      $header = 'From: '.$cfg['mail']['sender'] . "\r\n" .
        'Reply-To: '.$cfg['mail']['reply'] . "\r\n" .
        'X-Mailer: KiloCMS/' . KILOCMS_VERSION;
      return mail($receiver, $subject, $text, $header);
    }
    
    public static function getPostDate($date) {
      return strftime("%d.%m.%Y - %H:%M:%S", $date);
    }
  }
?>