<?php
  class admin_adminPage {
    private static function listPages($page = 1) {
      global $db, $cfg;
      page::addBC("List Pages", null, true);
      $start = ($page - 1) * $cfg['pagelistlimit'];
      $res = $db->query("SELECT * FROM pages ORDER BY page_numid ASC LIMIT $start, ".$cfg['pagelistlimit']);
      $tmp = "";
      page::setTitle("List of Pages");
      $tmp .= ("<table class=\"table\">");
      $tmp .= ("<tr><th>#</th><th>Internal Name</th><th>Title</th><th>Size</th><th>Options</th></tr>");
      while ($el = $res->fetchObject()) {        
        $tmp .= page::loadTemplate("pagelistentry.html", array(
          "id" => $el->page_numid,
          "page" => $el->page_id,
          "title" => $el->page_title,
          "size" => utils::bytes2string(strlen($el->page_content)),
          "dis" => ($el->page_id == "start" ? "disabled" : "")
          //"lastedit" => "On ".strftime("%d.%m.%Y - %H:%M", $tmp->page_lastediteddate)." by <a href=\"?p=profile&name=$el->page_lasteditedby\">$el->page_lasteditedby</a>"
        ));
      }
      
      $res = $db->query("SELECT COUNT(page_numid) FROM pages");
      $count = $res->fetchArray();
      $count = $count[0];
      
      $tmp .= ("</table>");
      $tmp .= (utils::getPageSelector($page, ceil($count / $cfg['pagelistlimit']), "?p=admin&sub=list&page=[[\$page]]"));
      
      page::addContents(page::loadTemplate("listpages.html", array(
        "list" => $tmp
      )));
    }
    
    private static function listNavi($page = 1) {
      global $db, $cfg;
      page::addBC("List Navigation", null, true);
      page::setTitle("Edit navigation");
      $start = ($page - 1) * $cfg['pagelistlimit'];
      $res = $db->query("SELECT * FROM navi ORDER BY navi_numid ASC LIMIT $start, ".$cfg['pagelistlimit']);
      $tmp = '<a href="?p=admin&sub=newnavi" class="btn btn-primary">New Link</a><br>&nbsp;';
      $tmp .= ("<table class=\"table\">");
      $tmp .= '<tr><th>#</th><th>Display text</th><th>Target</th><th>Options</th></tr>';
      $res_ = $db->query("SELECT COUNT(navi_numid) FROM navi");
      $count = $res_->fetchArray();
      $res_ = $db->query("SELECT * FROM navi ORDER BY navi_numid DESC LIMIT 1");
      $last = $res_->fetchObject();
      $count = $count[0];
      $num = 0;
      while ($el = $res->fetchObject()) {
        $num++;
        $tmp .= page::loadTemplate("navilistentry.html", array(
          "id" => $el->navi_numid,
          "title" => $el->navi_title,
          "tar" => ($el->navi_external == "true" ? "<a href=\"$el->navi_target\" target=\"_blank\" class=\"link-external\">$el->navi_target</a>" : "<a href=\"?p=$el->navi_target\" target=\"_blank\">$el->navi_target</a>"),
          "updis" => (($num == 1 and $page == 1) ? "disabled" : ""),
          "downdis" => ($el->navi_numid == $last->navi_numid ? "disabled" : "")
        ));
      }
      $tmp .= "</table>";
      
      $tmp .= (utils::getPageSelector($page, ceil($count / $cfg['pagelistlimit']), "?p=admin&sub=list&page=[[\$page]]"));
      
      page::addContents($tmp);
    }
    
    private static function checkToken() {
      $token = "";
      if (isset($_GET['token'])) {
        $token = $_GET['token'];
      }
      if (user::getToken() != $token) {
        header("HTTP/1.1 403 Forbidden");
        page::addContents("This may be caused due to you clicking an outdated link.");
        page::setTitle("invalid Token");
        return false;
      }
      return true;
    }
    
    public function run() {
      if (!(user::isLoggedIn() and user::info()->user_perms == "admin")) {
        header("HTTP/1.1 403 Forbidden");
        page::addContents("You have no permissions to access this page");
        page::setTitle("403 Forbidden");
        return;
      }
      
      global $db, $rootdir, $p, $cfg;
      page::addHead('<script type="text/javascript" src="js/KiloCMSadmin.js"></script>');
      page::addBC($cfg['project'], "?p=start");
      page::addBC("Admin Menu", "?p=admin&sub=");
      $sub = "";
      if (isset($_GET['sub'])) {
        $sub = $_GET['sub'];
      }
      switch ($sub) {
      case "newpage":
        $page = "";
        if (isset($_POST['page'])) $page = $_POST['page'];
        elseif (isset($_GET['page'])) $page = $_GET['page'];
        $res = $db->query("SELECT * FROM pages WHERE page_id = '".sql_escape($page)."'");
        if ($trash = $res->fetchObject()) {
          page::addContents("<div class=\"alert alert-error\">Page &quot;$page&quot; already exitst</div>");
          self::listPages();
        } else {
          page::addBC("New Page", null, true);
          page::setTitle("New Page");
          page::addContents(page::loadTemplate("editpage.html", array(
            "title" => $page,
            "content" => "",
            "sub" => "newpage",
            "page" => urlencode($page),
            "token" => user::getToken()
          )));
        }
        break;
      case "newpagepost":
        if (!self::checkToken()) break;
        $page = "";
        if (isset($_GET['page'])) $page = $_GET['page'];
        $res = $db->query("SELECT * FROM pages WHERE page_id = '".sql_escape($page)."'");
        if ($trash = $res->fetchObject()) {
          page::addContents("<div class=\"alert alert-error\">Page already exists</div>");
          self::listPages();
        }
        $db->query("INSERT INTO pages (page_id, page_title, page_content, page_lastediteddate, page_lasteditedby) VALUES ('".sql_escape($page)."', '".sql_escape($_POST['title'])."', '".sql_escape($_POST['content'])."', ".time().", '".user::info()->user_name."')");
        header("Location: ?p=admin&sub=list");
        break;
      case "editpage":
        $page = "";
        if (isset($_POST['page'])) $page = $_POST['page'];
        elseif (isset($_GET['page'])) $page = $_GET['page'];
        $res = $db->query("SELECT * FROM pages WHERE page_id = '".sql_escape($page)."'");
        if (!($tmp = $res->fetchObject())) {
          page::addContents("<div class=\"alert alert-error\">Page &quot;$page&quot; does not exist</div>");
          self::listPages();
        } else {
          page::addBC("List Pages", "?p=admin&sub=list");
          page::addBC("Edit Page", null, true);
          page::setTitle("New Page");
          page::addContents(page::loadTemplate("editpage.html", array(
            "title" => $tmp->page_title,
            "content" => $tmp->page_content,
            "sub" => "editpage",
            "page" => urlencode($page),
            "token" => user::getToken()
          )));
        }
        break;
      case "editpagepost":
        if (!self::checkToken()) break;
        $page = "";
        if (isset($_GET['page'])) $page = $_GET['page'];
        $res = $db->query("SELECT * FROM pages WHERE page_id = '".sql_escape($page)."'");
        if (!($trash = $res->fetchObject())) {
          page::addContents("<div class=\"alert alert-error\">Page does not exist</div>");
          self::listPages();
          break;
        }
        //$db->query("INSERT INTO pages (page_id, page_title, page_content, page_lastediteddate, page_lasteditedby) VALUES ('".sql_escape($page)."', '".sql_escape($_POST['title'])."', '".sql_escape($_POST['content'])."', ".time().", '".user::info()->user_name."')");
        $db->query("UPDATE pages SET page_title='".sql_escape($_POST['title'])."', page_content='".sql_escape($_POST['content'])."', page_lastediteddate=".time().", page_lasteditedby='".user::info()->user_name."' WHERE page_id = '".sql_escape($page)."'");
        header("Location: ?p=admin&sub=list");
        break;
      case "delpage":
        $page = "";
        if (isset($_GET['page'])) $page = $_GET['page'];
        if ($page == "start") {
          page::addContents("<div class=\"alert alert-error\">You cannot delete the Start Page</div>");
          self::listPages();
          break;
        }
        $res = $db->query("SELECT * FROM pages WHERE page_id = '".sql_escape($page)."'");
        if (!($trash = $res->fetchObject())) {
          page::addContents("<div class=\"alert alert-error\">Page does not exist</div>");
          self::listPages();
          break;
        }
        page::addBC("List Pages", "?p=admin&sub=list");
        page::addBC("Delete Page", null, true);
        page::addContents("<div class=\"alert alert-warning alert-block\"><strong>Do you really want to delete the page $page?</strong><br>&nbsp;<br><div class=\"btn-group\"><a href=\"?p=admin&sub=delpageconf&page=$page&token=".user::getToken()."\" class=\"btn btn-danger\">Yes</a><a href=\"?p=admin&sub=list\" class=\"btn btn-primary\">No</a></div></div>");
        page::setTitle("Confirm deletion");
        break;
      case "delpageconf":
        if (!self::checkToken()) break;
        $page = "";
        if (isset($_GET['page'])) $page = $_GET['page'];
        if ($page == "start") {
          page::addContents("<div class=\"alert alert-error\">You cannot delete the Start Page</div>");
          break;
        }
        $res = $db->query("SELECT * FROM pages WHERE page_id = '".sql_escape($page)."'");
        if (!($trash = $res->fetchObject())) {
          page::addContents("<div class=\"alert alert-error\">Page does not exist</div>");
          self::listPages();
          break;
        }
        $db->query("DELETE FROM pages WHERE page_id = '".sql_escape($page)."'");
        header("Location: ?p=admin&sub=list");
        break;
      case "list":
        $page = 1;
        if (isset($_GET['page']) and is_numeric($_GET['page'])) $page = $_GET['page'];
        if ($page < 1) $page = 1;
        self::listPages($page);
        break;
        
      case "listnavi":
        $page = 1;
        if (isset($_GET['page']) and is_numeric($_GET['page'])) $page = $_GET['page'];
        if ($page < 1) $page = 1;
        self::listNavi($page);
        break;
      case "newnavi":
        page::addBC("List Navigation", "?p=admin&sub=listnavi");
        page::addBC("New Navigation Link", null, true);
        page::addContents(page::loadTemplate("editnavilink.html", array(
          "sub" => "newnavi",
          "title" => "",
          "target" => "",
          "external" => "",
          "id" => "",
          "token" => user::getToken()
        )));
        break;
      case "newnavipost":
        if (!self::checkToken()) break;
        $db->query("INSERT INTO navi (navi_title, navi_target, navi_external) VALUES ('".sql_escape($_POST['title'])."', '".sql_escape($_POST['target'])."', '".(isset($_POST['external']) ? "true" : "false")."')");
        header("Location: ?p=admin&sub=listnavi");
        break;
      case "editnavi":
        page::addBC("List Navigation", "?p=admin&sub=listnavi");
        page::addBC("Edit Navigation Link", null, true);
        $res = $db->query("SELECT * FROM navi WHERE navi_numid = ".sql_escape($_GET['id']));
        if ($el = $res->fetchObject()) {
          page::addContents(page::loadTemplate("editnavilink.html", array(
            "sub" => "editnavi",
            "title" => $el->navi_title,
            "target" => $el->navi_target,
            "external" => ($el->navi_external == "true" ? " checked" : ""),
            "id" => $el->navi_numid,
            "token" => user::getToken()
          )));
        } else {
          page::addContents("<div class=\"alert alert-error\">This navi link does not exist</div>");
          self::listNavi();
        }
        break;
      case "editnavipost":
        if (!self::checkToken()) break;
        $res = $db->query("SELECT * FROM navi WHERE navi_numid = ".sql_escape($_GET['id']));
        if ($el = $res->fetchObject()) {
          $db->query("UPDATE navi SET navi_title='".sql_escape($_POST['title'])."', navi_target='".sql_escape($_POST['target'])."', navi_external='".(isset($_POST['external']) ? "true" : "false")."' WHERE navi_numid = ".sql_escape($_POST['id']));
          header("Location: ?p=admin&sub=listnavi");
        } else {
          page::addContents("<div class=\"alert alert-error\">This navi link does not exist</div>");
          self::listNavi();
        }
        
        break;
      case "delnavi":
        $id = -1;
        if (isset($_GET['id'])) $id = $_GET['id'];
        
        $res = $db->query("SELECT * FROM navi WHERE navi_numid = ".sql_escape($id)."");
        if (!($trash = $res->fetchObject())) {
          page::addContents("<div class=\"alert alert-error\">This navi link does not exist</div>");
          self::listNavi();
          break;
        }
        page::addBC("List Navigation", "?p=admin&sub=listnavi");
        page::addBC("Delete Navigation Link", null, true);
        page::addContents("<div class=\"alert alert-warning alert-block\"><strong>Do you really want to delete Link #$id?</strong><br>&nbsp;<br><div class=\"btn-group\"><a href=\"?p=admin&sub=delnaviconf&id=$id&token=".user::getToken()."\" class=\"btn btn-danger\">Yes</a><a href=\"?p=admin&sub=listnavi\" class=\"btn btn-primary\">No</a></div></div>");
        page::setTitle("Confirm deletion");
        break;
      case "delnaviconf":
        if (!self::checkToken()) break;
        $id = -1;
        if (isset($_GET['id'])) $id = $_GET['id'];
        
        $res = $db->query("SELECT * FROM navi WHERE navi_numid = ".sql_escape($id));
        if (!($trash = $res->fetchObject())) {
          page::addContents("<div class=\"alert alert-error\">This Navi Link does not exist</div>");
          self::listNavi();
          break;
        }
        $db->query("DELETE FROM navi WHERE navi_numid = ".sql_escape($id));
        header("Location: ?p=admin&sub=listnavi");
        break;
      case "naviup":
        $id = -1;
        if (isset($_GET['id'])) $id = $_GET['id'];
        $id = sql_escape($id);
        
        $res = $db->query("SELECT * FROM navi WHERE navi_numid < $id ORDER BY navi_numid DESC LIMIT 0, 1"); 
        if (!$el = $res->fetchObject()) {
            break;
        }
        
        $db->query("UPDATE navi SET navi_numid=-1 WHERE navi_numid=$id");
        $db->query("UPDATE navi SET navi_numid=$id WHERE navi_numid=$el->navi_numid");
        $db->query("UPDATE navi SET navi_numid=$el->navi_numid WHERE navi_numid=-1");
        header("Location: ?p=admin&sub=listnavi");
        break;
      case "navidown":
        $id = -1;
        if (isset($_GET['id'])) $id = $_GET['id'];
        $id = sql_escape($id);
        
        $res = $db->query("SELECT * FROM navi WHERE navi_numid > $id ORDER BY navi_numid ASC LIMIT 0, 1"); 
        if (!$el = $res->fetchObject()) {
            break;
        }
        
        $db->query("UPDATE navi SET navi_numid=-1 WHERE navi_numid=$id");
        $db->query("UPDATE navi SET navi_numid=$id WHERE navi_numid=$el->navi_numid");
        $db->query("UPDATE navi SET navi_numid=$el->navi_numid WHERE navi_numid=-1");
        header("Location: ?p=admin&sub=listnavi");
        break;
      default:
        page::addBC("Overview", null, true);
        page::setTitle("Admin Menu");
        page::addContents(page::loadTemplate("admin-overview.html"));
        break;
      }
    }
  }
  pageapi::registerPage("admin", new admin_adminPage());
?>