<?php
  class pageapi {
    private static $pages = array();
    public static function callPage($name) {
      if (isset(self::$pages[$name])) {
        self::$pages[$name]->run();
        return true;
      }
      return false;
    }
    
    public static function registerPage($name, $page) {
      self::$pages[$name] = $page;
    }
  }
?>