<?php
    class page {
        private static $content = "";
        private static $title = "unnamed";
        private static $navi = "";
        private static $head = "";
        private static $breadcrumbs = "";
        private static $disable = false;
        
        public static function addHead($s) {
            self::$head .= $s;
        }
        
        public static function disableEverything($v = true) {
            self::$disable = $v;
        }
        
        public static function addBC($title, $url, $current = false) {
            self::addBreadcrumb($title, $url, $current);
        }
        
        public static function addBreadcrumb($title, $url, $current = false) {
            if (self::$breadcrumbs != "") {
                 self::$breadcrumbs .= '<span class="divider">&raquo;</span></li>';
            }
            if (!$current)
                self::$breadcrumbs .= "<li><a href=\"$url\">$title</a>";
            else
                self::$breadcrumbs .= "<li class=\"active\">$title</li>";
        }
        
        public static function addContents($s) {
            self::$content .= $s;
        }
        
        public static function setTitle($s) {
            if ($s == null) return;
            self::$title = $s;
        }
        
        public static function loadTemplate($t, $r = array()) {
            global $rootdir;
            $tmpl = file_get_contents($rootdir."template".DIRECTORY_SEPARATOR.$t);
            foreach($r as $k => $v) {
                $tmpl = str_replace("[[\$".$k."]]", $v, $tmpl);
            }
            return $tmpl;
        }
        
        public static function output() {
            global $cfg, $p, $db, $kilocms_version, $pagename;
            $act = "";
            if ($p == "login") {
                $act = " class=\"active\"";
            }
            $act2 = "";
            if ($p == "signup") {
                $act2 = " class=\"active\"";
            }
            $user = "";
            $user .= "<li$act><a href=\"?p=login&r=".urlencode($pagename)."\">Log in</a></li>";
            $user .= "<li$act2><a href=\"?p=signup&r=".urlencode($pagename)."\">Register</a></li>";
            if (self::$disable) $user = "";
            $adm = "";
            if (user::isLoggedIn() and !self::$disable) {
                if ($p == "account") {
                    $act = " class=\"active\"";
                }
                $adm = "";
                if (user::info()->user_perms == "admin") {
                    $adm = self::loadTemplate("admbtn.html");
                }
                
                $user = page::loadTemplate("userbtn.html", array(
                    "name" => user::info()->user_name,
                ));
            }
            
            $navi = "";
            if (!self::$disable) {
                $res = $db->query("SELECT * FROM navi");
                while ($el = $res->fetchObject()) {
                    if ($el->navi_external == "true")
                        $navi .= "<li><a href=\"$el->navi_target\" target=\"_blank\">$el->navi_title</a></li>";
                    else {
                        $act = "";
                        if ($el->navi_target == $p) {
                            $act = " class=\"active\"";
                        }
                        $navi .= "<li$act><a href=\"?p=$el->navi_target\">$el->navi_title</a></li>";
                    }
                }
            }
            echo stripslashes(self::loadTemplate("main.html", array(
                "content" => self::$content,
                "navi" => $navi,
                "head" => self::$head,
                "title" => self::$title,
                "project" => $cfg['project'],
                "user" => $adm.$user."</ul>",
                "bread" => self::$breadcrumbs,
                "version" => KILOCMS_VERSION
            )));
        }
    }
?>