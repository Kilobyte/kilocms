<?php
  session_start();
  if (!isset($_SESSION['kilocms_name'])) $_SESSION['kilocms_name'] = "";
  class user {
    private static $user = null;
    public static function isLoggedIn() {
      return $_SESSION['kilocms_name'] != "";
    }
    
    public static function setUser($u) {
      self::$user = $u;
    }
    
    public static function getToken() {
      if (!isset($_SESSION['kilocms_token'])) {
        $_SESSION['kilocms_token'] = hash("sha512", time());
      }
      return $_SESSION['kilocms_token'];
    }
    
    public static function info() {
      return self::$user;
    }
    
    public static function hasPerm($perm) {
      $perms = array("user" => 1, "moderator" => 2, "writer" => 3, "admin" => 4, "banned" => -1);
      if (!self::isLoggedIn()) return ($perms[$perm] <= 0);
      return $perms[$perm] <= $perms[self::info()->user_perms];
    }
    
    public static function init() {
      global $db;
      if (self::isLoggedIn()) {
        $res = $db->query("SELECT * FROM users WHERE user_name = '".sql_escape($_SESSION['kilocms_name'])."'");
      	if ($tmp = $res->fetchObject()) 
          self::$user = $tmp;
      }
    }
  }
  
  class pageLoginLogout {
    public function run() {
      global $db, $cfg;
      $r = "start";
      $r2 = "";
      if (isset($_GET['r'])) {
        $r = $_GET['r'];
        $r2 = "&r=".urlencode($r);
      }
      //print($_GET['p']);
      switch($_GET['p']) {
      case "login":
        page::addContents(page::loadTemplate("login.html", array("return" => $r2)));
        page::setTitle("Login");
        break;
      case "loginpost":
        //print("test");
      	$res = $db->query("SELECT * FROM users WHERE user_name = '".sql_escape($_POST['user'])."'");
      	if ($tmp = $res->fetchObject()) {
          //die("Out of order");
          $hash = hash_hmac("sha512", $_POST['pass'], $tmp->user_passsalt);
          if ($hash == $tmp->user_pass) {
            if ($tmp->user_active == "false") {
              page::setTitle("Login");
              page::addContents("<div class=\"alert alert-error\">You need to activate your account first</div>");
              page::addContents(page::loadTemplate("login.html"));
              break;
            }
            header("Location: ?p=".$r);
            $_SESSION['kilocms_name'] = $tmp->user_name;
            $_SESSION['kilocms_token'] = hash("sha512", time());
            user::setUser($tmp);
          } else {
            page::setTitle("Login");
            page::addContents("<div class=\"alert alert-error\">Sorry, But that was no valid Login Data </div>");
            page::addContents(page::loadTemplate("login.html", array("return" => $r2)));
          }
      	} else {
      	  page::setTitle("Login");
      	  page::addContents("<div class=\"alert alert-error\">Sorry, But that was no valid Login Data </div>");
      	  page::addContents(page::loadTemplate("login.html", array("return" => $r2)));
      	}
      	break;
      case "logout":
        $_SESSION['kilocms_name'] = "";
        //&r=".urlencode($pagename)."
        header("Location: ?p=".$r);
      	break;
      case "signup":
        page::addContents(page::loadTemplate("signup.html", array("return" => $r2)));
        page::setTitle("Register");
        break;
      case "signuppost":
        $name = $_POST['user'];
        $pw1 = $_POST['pw1'];
        $pw2 = $_POST['pw2'];
        $mail = $_POST['mail'];
        if (!($name != "" and $pw1 != "" and $mail != "")) {
          page::addContents("<div class=\"alert alert-error\">Please fill in all fields</div>");
          page::addContents(page::loadTemplate("signup.html", array("return" => $r2)));
          page::setTitle("Register");
          break;
        }
        if ($pw1 != $pw2) {
          page::addContents("<div class=\"alert alert-error\">The entered password do not match</div>");
          page::addContents(page::loadTemplate("signup.html", array("return" => $r2)));
          page::setTitle("Register");
          break;
        }
        if (preg_match("/^[^ \/\\:\*@]+@[^ \/\\:\.\*@]+[^ \/\\:\*@]+$/", $mail) != 1) {
          page::addContents("<div class=\"alert alert-error\">Invalid EMail Address</div>");
          page::addContents(page::loadTemplate("signup.html", array("return" => $r2)));
          page::setTitle("Register");
          break;
        }
        $res = $db->query("SELECT * FROM users WHERE user_name = '".sql_escape($name)."' OR user_email = '".sql_escape($mail)."'");
        if ($trash = $res->fetchObject()) {
          page::addContents("<div class=\"alert alert-error\">Mail address or username already in use</div>");
          page::addContents(page::loadTemplate("signup.html", array("return" => $r2)));
          page::setTitle("Register");
          break;
        }
        $salt = utils::getSecureRandom();
        
        
        if ($cfg['mail']['activation']) {
          $active = "false";
          $code = hash_hmac("sha512", time(), utils::getSecureRandom());
          if (utils::sendMail("Welcome to ".$cfg['project'], "register.txt", $mail, array(
            "user" => $name,
            "url" => "http://".$cfg['domain']."/".$cfg['path'],
            "activatelink" => "http://".$cfg['domain']."/".$cfg['path']."?p=activate&user=".urlencode($name)."&code=".urlencode($code),
            "project" => $cfg['project']
          ))) {
            
          } else {
            page::addContents("<div class=\"alert alert-error\">Could not send registration E-Mail</div>");
            break;
          }
        } else {
          $active = "true";
          $code = "";
        }        
        
        $db->query("INSERT INTO users (user_name, user_pass, user_email, user_active, user_activatecode, user_passsalt) VALUES ('".sql_escape($name)."', '".hash_hmac("sha512", $pw1, $salt)."', '".sql_escape($mail)."', '$active', '".sql_escape($code)."', '".sql_escape($salt)."')");
        header("Location: ?p=login".$r2);
        break;
      case "activate":
        $res = $db->query("UPDATE users SET user_active='true', user_activatecode='' WHERE user_name = '".sql_escape($_GET['user'])."' and user_activatecode='".sql_escape($_GET['code'])."'");
        if ($res->affected_rows == 1) {
          page::addContents("<div class=\"alert alert-success\"><strong>Activation succeeded.</strong> You may now log in</div>");
        } else {
          page::addContents("<div class=\"alert alert-error\">Invalid Activation code.</div>");
        }
        break;
      }
    }
  }
  $p = new PageLoginLogout();
  pageapi::registerPage("login", $p);
  pageapi::registerPage("logout", $p);
  pageapi::registerPage("loginpost", $p);
  pageapi::registerPage("signup", $p);
  pageapi::registerPage("signuppost", $p);
  pageapi::registerPage("activate", $p);
?>