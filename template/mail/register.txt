Welcome to [[$project]]!

You just signed up on [[$url]] as [[$user]].

To confirm your account open this link: [[$activatelink]]

Cheers, your [[$project]] Team